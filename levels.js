var LEVELS = {
  "Tutorial 1": {
    "about": "It's toy soldiers versus ants!",
    "setting": "land",
    "time": 60,
    "required": null,
    "player": {
      "coins": 10,
      "cost": {
        "soldier": 1
      },
      "placed": []
    },
    "enemy": {
      "placed": [
        {
          "type": "ant",
          "x": 0.75,
          "y": 0.25
        },
        {
          "type": "ant",
          "x": 0.75,
          "y": 0.5
        },
        {
          "type": "ant",
          "x": 0.75,
          "y": 0.75
        }
      ]
    }
  },
  "Tutorial 2": {
    "about": "Now it's tanks (and soldiers) versus chip bags (and ants)!",
    "setting": "land",
    "time": 120,
    "required": "Tutorial 1",
    "player": {
      "coins": 30,
      "cost": {
        "soldier": 1,
        "tank": 5
      },
      "placed": []
    },
    "enemy": {
      "placed": [
        {
          "type": "ant",
          "x": 0.666,
          "y": 0.25
        },
        {
          "type": "ant",
          "x": 0.666,
          "y": 0.5
        },
        {
          "type": "ant",
          "x": 0.666,
          "y": 0.75
        },
        {
          "type": "chipbag",
          "x": 0.85,
          "y": 0.375
        },
        {
          "type": "chipbag",
          "x": 0.85,
          "y": 0.675
        }
      ]
    }
  },
  "Tutorial at Sea": {
    "about": "Aircraft carriers and bombers!",
    "setting": "water",
    "time": 120,
    "required": "Tutorial 2",
    "player": {
      "coins": 60,
      "cost": {
        "carrier": 20,
        "plane": 10
      },
      "placed": []
    },
    "enemy": {
      "placed": [
        {
          "type": "gunship",
          "x": 0.23046875,
          "y": 0.45703125
        },
        {
          "type": "support",
          "x": 0.6875,
          "y": 0.451171875
      }
      ]
    }
  },
  "Where to start?": {
    "about": "Ten coins, ten seconds.  Make them count!",
    "setting": "land",
    "time": 10,
    "required": "Tutorial at Sea",
    "player": {
      "coins": 10,
      "cost": {
        "soldier": 2
      },
      "placed": []
    },
    "enemy": {
      "placed": [
        {
          "type": "ant",
          "x": 0.4765625,
          "y": 0.009765625
        },
        {
          "type": "chipbag",
          "x": 0.009765625,
          "y": 0.01171875
        },
        {
          "type": "chipbag",
          "x": 0.861328125,
          "y": 0.01171875
        },
        {
          "type": "ant",
          "x": 0.859375,
          "y": 0.400390625
        },
        {
          "type": "ant",
          "x": 0.0234375,
          "y": 0.439453125
        },
        {
          "type": "chipbag",
          "x": 0.017578125,
          "y": 0.8671875
        },
        {
          "type": "chipbag",
          "x": 0.869140625,
          "y": 0.87109375
        },
        {
          "type": "ant",
          "x": 0.498046875,
          "y": 0.869140625
        }
      ]
    }
  },
  "Rescue Mission 1": {
    "about": "Save the wounded soldier!",
    "setting": "land",
    "time": 0,
    "required": "Where to start?",
    "player": {
      "coins": 10,
      "cost": {
        "medic": 5
      },
      "placed": []
    },
    "enemy": {
      "placed": [
        {
          "type": "ant",
          "x": 0.41015625,
          "y": 0.349609375
        },
        {
          "type": "woundedsoldier",
          "x": 0.44921875,
          "y": 0.388671875
        },
        {
          "type": "ant",
          "x": 0.46875,
          "y": 0.4609375
        },
        {
          "type": "ant",
          "x": 0.537109375,
          "y": 0.33203125
        }
      ]
    }
  }
};


function populatelevels(into, collection) {
  for(level in collection) {
    if(level === "_template") continue;
    var spec = encodeURIComponent(JSON.stringify(collection[level]));
    var about = collection[level].about;
    var title = encodeURIComponent(level);
    var a = document.createElement("a");
    if(collection[level].required &&
       (!DATA || !DATA.complete ||
        DATA.complete.indexOf(collection[level].required) < 0)) {
      // required level has not been completed
      a.innerHTML += level + "<br>";
      a.innerHTML += [
        "<span class='about'>(complete '",
        collection[level].required,
        "' to unlock)</span>"
      ].join("");
      a.className = "disabled";
      a.href = "#";
    } else {
      // "&star; &starf" // open/closed star
      if(DATA && DATA.complete && DATA.complete.indexOf(level) >= 0) {
        a.innerHTML += "&check; ";
        a.className = "complete";
      }
      a.innerHTML += level + "<br>";
      a.innerHTML += "<span class='about'>" + (about || "&nbsp;") + "</span>";
      a.href = "play.html?title=" + title + "&level=" + spec;
    }
    into.appendChild(a);
  }
}

window.addEventListener("load", function() {
  if(typeof data_load === "function") {
    data_load();
  }

  var div = document.querySelector("#levels");
  if(div) {
    populatelevels(div, LEVELS);
  }

  div = document.querySelector("#customlevels");
  if(div && DATA && DATA.custom && Object.keys(DATA.custom).length) {
    var h2 = document.createElement("h2");
    h2.textContent = "Custom Levels";
    div.appendChild(h2);
    if(div) {
      populatelevels(div, DATA.custom);
    }
  }

  var spectext = document.querySelector("#customlevel");
  var loadspec = document.querySelector("#loadcustomlevel");
  if(spectext && loadspec) {
    loadspec.addEventListener("click", function(e) {
      try {
        var spec = encodeURIComponent(JSON.stringify(JSON.parse(spectext.value)));
        window.location = "play.html?level=" + spec;
      } catch(err) {
        alert("Error loading level.\n" + err);
      }
    });
  }
});

// store game data in browser
// note: both sessionStorage and localStorage are used.
// localStorage acts differently for local files than online;
// by using both this way, save data from file:/// locations
// can follow the player across the game's various pages.

// note: this is for illustration only.
// always data_load() before using DATA, which may be empty {}
// when DATA has been modified, do not forget to data_save()
DATA = {
  complete: [], // "Tutorial 1", "Level Name", ...
  custom: {}    // "levelname": { spec }, ...
};

//var STORE = (location.protocol === "file:" ? sessionStorage : localStorage);

window.addEventListener("load", function(e) {
  // load into memory right away
  data_load();

  // set up data upload (if available on page)
  var input = document.querySelector("input.upload");
  if(input) {
    input.addEventListener("change", function(e) {
	  var reader = new FileReader();
	  reader.addEventListener("load", function(e) {
        var data = null;
        try {
	      data = JSON.parse(e.target.result);
          if(confirm("Confirm: replace saved data?")) {
            DATA = data;
            localStorage.setItem("deathbugsdata", JSON.stringify(DATA));
            sessionStorage.setItem("deathbugsdata", JSON.stringify(DATA));
          }
        } catch(err) {
          alert("Error parsing file; data was not accepted.\n", err);
        }
	  });
	  reader.readAsText(this.files[0]);
    });
  }

  // set up any buttons
  var btn = null;
  btn = document.querySelector("#data_download");
  if(btn) btn.addEventListener("click", data_download);
  btn = document.querySelector("#data_upload");
  if(btn) btn.addEventListener("click", data_upload);
  btn = document.querySelector("#data_clear");
  if(btn) btn.addEventListener("click", data_clear);
  btn = document.querySelector("#data_clearprogress");
  if(btn) btn.addEventListener("click", data_clearprogress);
  btn = document.querySelector("#data_load");
  if(btn) btn.addEventListener("click", data_load);
  btn = document.querySelector("#data_save");
  if(btn) btn.addEventListener("click", data_save);
});

// load saved data into memory
function data_load() {
  try {
    var data = sessionStorage.getItem("deathbugsdata");
    if(!data) {
      data = localStorage.getItem("deathbugsdata");
    }
    DATA = JSON.parse(data || "{}");
  } catch(err) {
    console.error("failed to load save data", err);
  }
  data_save(); // sync up with wherever we successfully loaded from
}

// save data to browser storage
function data_save() {
  localStorage.setItem("deathbugsdata", JSON.stringify(DATA));
  sessionStorage.setItem("deathbugsdata", JSON.stringify(DATA));
}

// erase data from browser storage
function data_clear() {
  if(confirm("Confirm: Clear all saved data?")) {
    localStorage.removeItem("deathbugsdata");
    sessionStorage.removeItem("deathbugsdata");
  }
}

// erase just completed level data from browser storage
function data_clearprogress() {
  if(confirm("Confirm: Erase completed-level progress?\n" +
             "Custom levels will stay saved.")) {
    data_load();
    DATA.complete = [];
    data_save();
  }
}

// download save data to file
function data_download() {
  var a = null;
  a = document.createElement("a");
  a.className = "download hidden";
  a.download = "savedata.json";
  a.href = "data:text/plain;base64," + btoa(JSON.stringify(DATA, null, 2));
  document.body.appendChild(a);
  document.querySelector("a.download").click();
  document.body.removeChild(a);
}

// replace save data with uploaded file
function data_upload() {
  document.querySelector("input.upload").click();
}

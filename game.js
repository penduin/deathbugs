var GAME = {
  config: {
    ticks_per_second: 24,
    shoot_interval: 50,
    bunkershoot_interval: 240,
    bugbag_interval: 80,
    tanktrooper_interval: 240,
    carrierplane_interval: 240,
    explosion_frames: 10,
    smoke_frames: 15,
    bug_grow_frames: 10,
    tank_hits: 5,
    soldier_hits: 1,
    medic_hits: 1,
    ship_hits: 6,
    carrier_hits: 15,
    carrier_max_planes: 1,
    plane_fuel: 360,
    plane_hits: 2,
    support_hits: 2,
    gunship_hits: 6,
    base_radius_scale: 0.5
  },

  running: false,
  justhitpause: false,
  level: null,

  ctx: null,
  sprites: [],
  shots: [],
  drag: {
    element: null,
    sprite: null,
    x: 0,
    y: 0
  },
  ticks: 0,
  frametime: 0,
  lastframe: 0,
  backup: {
    sprites: ""
  },

  over: false,
  win: {
    elm: null,
    units: [],  // when these id's are all gone, then player wins
    time: false // player wins when time runs out
  },
  lose: {
    elm: null,
    units: [],  // when these id's are gone and funds are...
    funds: 0,   // ...below this, then player loses
    time: false // player loses when time runs out
  },
  funds: 0,
  funds_elm: null,
  msec_left: 0,
  end_time: 0,
  timer_elm: null
};
var LEVEL = {};
var TO_RADIANS = Math.PI / 180;

window.addEventListener("load", function() {
  init();
});

function init() {
  var canvas = document.querySelector("canvas");
  GAME.ctx = canvas.getContext("2d");
  GAME.funds_elm = document.querySelector("#funds");
  GAME.timer_elm = document.querySelector("#time");
  GAME.win.elm = document.querySelector("#win");
  GAME.lose.elm = document.querySelector("#lose");

  // drag-and-drop images to canvas
  document.addEventListener("dragstart", dragstart);
  document.addEventListener("dragover", dragover);
  document.addEventListener("dragend", dragend);
  document.addEventListener("drop", drop);
  document.addEventListener("touchstart", dragstart);
//  document.addEventListener("touchmove", touchmove);
  document.addEventListener("touchend", drop);

  // drag around on canvas itself
  canvas.addEventListener("mousedown", mousedown);
  canvas.addEventListener("mousemove", mousemove);
  canvas.addEventListener("mouseup", mouseup);
  canvas.addEventListener("mouseout", mouseout);

  document.querySelectorAll("img").forEach(function(img) {
    img.addEventListener("click", imgtap);
  });

  // prevent selection
  document.addEventListener("selectstart", function(e) {
    e.preventDefault();
    return false;
  });

  hookup(document.querySelector("#pause"), "click", pause);
  hookup(document.querySelector("#run"), "click", run);
  hookup(document.querySelector("#tick"), "click", tick);
  hookup(document.querySelector("#save"), "click", save);
  hookup(document.querySelector("#load"), "click", load);
  hookup(document.querySelector("#clear"), "click", clear);
  if(document.querySelector("#baserange")) {
    hookup(document.querySelector("#baserange"), "change", basechange);
  }
  hookup(document.querySelector("#level"), "change", selectlevel);

  // select level
  selectlevel();

  // load level data
  loadlevel();

  window.requestAnimationFrame(render);
}

function hookup(element, event, func) {
  if(element) {
    element.addEventListener(event, func);
  }
}

function loadlevel(spec, name) {
  if(spec) {
    LEVEL = spec;
  } else {
    var GET = {};
    window.location.search.replace("?", "").split("&").forEach(function(s) {
	  GET[s.split("=")[0]] = decodeURIComponent(s.split("=")[1]);
    });
    if(!GET.level) return; // || !GET.title
    try {
      LEVEL = JSON.parse(GET.level);
    } catch(err) {
      console.error("could not parse level", err);
    }

    // don't do title/costs if we're loading from the editor (spec,name passed)
    document.title = "Death Bugs : " + (GET.title || "");
    LEVEL.title = (GET.title || "");
    setupcosts();
  }

  document.querySelector("#level").value = LEVEL.setting;
  selectlevel(null, true);

  if(LEVEL.enemy && LEVEL.enemy.placed) {
    LEVEL.enemy.placed.forEach(function(sprite) {
      console.log("adding an enemy sprite", sprite);
      // be flashy if no type
      var img = document.getElementById(sprite.type || "boom");

      // inherit any additional params, set up img,x,y,w,h
      var obj = JSON.parse(JSON.stringify(sprite));
      obj.img = img;
      delete obj.type;
      obj.x = sprite.x * GAME.ctx.canvas.width;
      obj.y = sprite.y * GAME.ctx.canvas.height;
      obj.w = img.naturalWidth;
      obj.h = img.naturalHeight;

      // add the sprite
      GAME.sprites.push(obj);

      // set up win conditions
      if(GAME.win.units.indexOf(sprite.type) < 0) {
        GAME.win.units.push(sprite.type);
      }
    });
  }
  if(LEVEL.player && LEVEL.player.placed) {
    LEVEL.player.placed.forEach(function(sprite) {
      console.log("adding a sprite", sprite);
      // be flashy if no type
      var img = document.getElementById(sprite.type || "boom");

      // inherit any additional params, set up img,x,y,w,h
      var obj = JSON.parse(JSON.stringify(sprite));
      obj.img = img;
      delete obj.type;
      obj.x = sprite.x * GAME.ctx.canvas.width;
      obj.y = sprite.y * GAME.ctx.canvas.height;
      obj.w = img.naturalWidth;
      obj.h = img.naturalHeight;

      // add the sprite
      GAME.sprites.push(obj);

      // set up lose conditions
      if(GAME.lose.units.indexOf(sprite.type) < 0) {
        GAME.lose.units.push(sprite.type);
      }
    });
  }

  GAME.end_time = GAME.msec_left = 0;
  if(GAME.timer_elm) {
    window.requestAnimationFrame(function(now) {
      if(LEVEL.time) {
        document.querySelector("#clock").className = "";
        GAME.end_time = now + (LEVEL.time * 1000);
        GAME.msec_left = (LEVEL.time * 1000);
      } else {
        document.querySelector("#clock").className = "hidden";
      }
    });
  }

  LEVEL.play = true;
}

function setupcosts() {
  if(LEVEL.player) {
    GAME.funds = LEVEL.player.coins;
  };
  updatefunds();

  var items = document.querySelectorAll("img[draggable=true]");
  items.forEach(function(item) {
    var id = item.id;
    var cont = item.parentElement;
    var span = document.createElement("span");
    var img = document.createElement("img")
    var cost = 0;
    if(LEVEL.player && LEVEL.player.cost) {
      cost = LEVEL.player.cost[id] || 0;
    }
    span.className = "buy";
    if(!cost) {
      span.className += " hidden";
    } else {
      // also set up win/loss condition checks
      if(GAME.lose.units.indexOf(id) < 0) {
        GAME.lose.units.push(id);
      }
      // a tank is still alive if it's become a bunkered soldier
      if(id === "tank" && GAME.lose.units.indexOf("tank") < 0) {
        GAME.lose.units.push("bunkeredsoldier");
      }
      // a wrecktank is still alive if it's become a bunkered soldier
      if(id === "wrecktank" && GAME.lose.units.indexOf("wrecktank") < 0) {
        GAME.lose.units.push("bunkeredsoldier");
      }
      // medics are available to revive injured soldiers
      if(id === "medic" &&
         GAME.lose.units.indexOf("soldier") < 0) {
        GAME.lose.units.push("soldier");
      }
    }
    span.appendChild(item);
    span.appendChild(document.createElement("br"));
    img.src = "gpx/coin.png";
    img.className = "coin";
    img.draggable = false;
    span.appendChild(img);
    span.appendChild(document.createTextNode(cost.toString()));
    cont.appendChild(span);
  });
}

function updatetimer(now) {
  if(GAME.over) return;
  if(GAME.timer_elm && GAME.end_time) {
    var remaining = 0;
    if(GAME.running) {
      remaining = Math.floor((GAME.end_time - now) / 1000);
    } else {
      remaining = Math.floor(GAME.msec_left / 1000);
    }
    GAME.timer_elm.textContent = [
      Math.floor(remaining / 60).toString().padStart(2, "0"),
      (remaining % 60).toString().padStart(2, "0")
    ].join(":");
    if(remaining < 11) {
      if(Math.floor(remaining) % 2) {
        GAME.timer_elm.parentElement.className = "";
      } else {
        GAME.timer_elm.parentElement.className = "hurry";
      }
    }
    if(remaining < 0) {
      GAME.timer_elm.textContent = "TIME UP!";
      GAME.timer_elm.parentElement.className = "hurry";
    }
  }
}

function updatefunds(amount) {
  GAME.funds += (amount || 0);
  if(GAME.funds_elm) {
    GAME.funds_elm.textContent = GAME.funds.toString();
  }
  if(amount) {
    var cost = document.createElement("div");
    cost.className = "cost";
    document.body.appendChild(cost);
    cost.textContent = amount.toString();
    if(amount > 0) {
      cost.textContent = "+" + cost.textContent;
      cost.className += " gain";
    }
    setTimeout(function() {
      cost.className += " fade";
    }, 10);
    setTimeout(function() {
      document.body.removeChild(cost);
    }, 2000);
  }
}

function spend(amount) {
  //console.log("spending ", amount);
  if(GAME.funds < amount) {
    if(GAME.funds_elm) {
      GAME.funds_elm.parentElement.className = "notenough";
      setTimeout(function() {
        GAME.funds_elm.parentElement.className = "";
      }, 500);
    }
    return false;
  }
  updatefunds(0 - amount);
  return true;
}

function checkwin() {
  if(GAME.over) return;
  if(!GAME.running) return;
  if(!LEVEL.player) return;
  var notyetwon = false;
  var notyetlost = false;

  GAME.sprites.every(function(sprite) {
    if(GAME.win.units.indexOf(sprite.img.id) >= 0) {
      // found an enemy still alive; no win yet
      notyetwon = true;
    }
    if(GAME.lose.units.indexOf(sprite.img.id) >= 0) {
      // found an ally still alive; no loss yet
      notyetlost = true;
    }
    return true;
    //return !notyetwon || !notyetlost;
  });

  if(!notyetwon) {
    gameover(true);
    return;
  }
  //console.log("notyetlost", notyetlost);
  if(!notyetlost) {
    // if we can afford the cheapest unit, there's still a chance to win
    var needfunds = 99999;
    GAME.lose.units.forEach(function(id) {
      if(LEVEL.player && LEVEL.player.cost) {
        needfunds = Math.min(needfunds, LEVEL.player.cost[id] || 99999);
      }
    });
//    console.log("notyetlost", notyetlost, "needfunds", needfunds,
//                "GAME.funds", GAME.funds, ">", needfunds > GAME.funds);
    if(needfunds > GAME.funds) {
      gameover();
      return;
    }
  }
}

function savewon(title) {
  if(!title) return;
  if(typeof data_load === "function") {
    data_load();
  }
  DATA = DATA || {};
  DATA.complete = DATA.complete || [];
  if(DATA.complete.indexOf(title) < 0) DATA.complete.push(title);
  if(typeof data_save === "function") {
    data_save();
  }
}

function gameover(win) {
  GAME.running = false;
  GAME.over = true;
  if(win) {
    if(GAME.win.elm) {
      GAME.win.elm.className = "shown";
    }
    savewon(LEVEL.title);
  } else {
    if(GAME.lose.elm) {
      GAME.lose.elm.className = "shown";
    }
  }
}

function spawn(type, x, y, extra) {
  var nope = false;
  if(LEVEL.player && LEVEL.player.cost) {
    if(!spend(LEVEL.player.cost[type])) {
      console.log("not enough coins to spawn", type);
      nope = true;
    }
  }
  var img = document.getElementById(type);
  if(!nope) {
    var obj = {
      img: img,
      x: x,
      y: y,
      w: img.naturalWidth,
      h: img.naturalHeight
    };
    if(extra) {
      for(prop in extra) {
        obj[prop] = extra[prop];
      }
    }
    GAME.sprites.push(obj);
  }
}

// draw everything to screen
function render(time) {
  window.requestAnimationFrame(render);

  // tick if needed
  if(GAME.running) {
    var logicTickWait = Math.floor(1000 / GAME.config.ticks_per_second);
    if(GAME.frametime) {
      GAME.frametime += time - GAME.lastframe;
    } else {
      GAME.frametime = time - GAME.lastframe;
    }
    while(GAME.frametime >= logicTickWait) {
      GAME.frametime -= logicTickWait;
      tick(time);
    }
  } else {
    GAME.lastframe = 0;
    GAME.frametime = 0;
  }

  // background
  switch(GAME.level) {
  case "land":
    GAME.ctx.fillStyle = "#321";
    break;
  case "water":
    GAME.ctx.fillStyle = "#123";
    break;
  default:
    GAME.ctx.fillStyle = "#222";
  }
  GAME.ctx.fillRect(0, 0, GAME.ctx.canvas.width, GAME.ctx.canvas.height);

  switch(GAME.level) {
  case "water":
    // special sort according to extra "data-height" hint based on type
    GAME.sprites.sort(function(a,b) {
      if(a.img.dataset.height && b.img.dataset.height) {
        if(a.img.dataset.height != b.img.dataset.height) {
          return a.img.dataset.height - b.img.dataset.height;
        }
      }
      return (a.y || 0) - (b.y || 0);
    });
    break;
  case "land":
  default:
    // regular sort, lower on screen == shown in front == drawn last
    GAME.sprites.sort(function(a, b) {
      return (a.y || 0) - (b.y || 0);
    });
    break;
  }

  // active sprites
  GAME.sprites.forEach(function(sprite) {
    GAME.ctx.save();
    GAME.ctx.translate(sprite.x + (sprite.w / 2),
                       sprite.y + (sprite.h / 2));
    //GAME.ctx.rotate(Math.sin(time / 100) / 10);
    if(sprite.velX && sprite.velX < 0) {
      // face left if moving left
      GAME.ctx.scale(-1, 1);
    }
    GAME.ctx.rotate((sprite.rot || 0) * TO_RADIANS);
    if(sprite.size || sprite.size === 0) {
      GAME.ctx.scale((sprite.size), (sprite.size));
    }

    GAME.ctx.globalAlpha = (sprite.solid || 1);
    GAME.ctx.translate(-sprite.w / 2, -sprite.h / 2);
    if(sprite.radius) {
      GAME.ctx.save();
      GAME.ctx.strokeStyle = "teal";
      GAME.ctx.globalAlpha = (Math.sin(time / 200) + 1) / 2;
      GAME.ctx.lineWidth = 4;
      GAME.ctx.beginPath();
      GAME.ctx.arc(sprite.img.naturalWidth/2, sprite.img.naturalHeight/2,
                   (sprite.radius
                    * sprite.img.naturalWidth
                    * GAME.config.base_radius_scale),
                   0, 2 * Math.PI);
      GAME.ctx.stroke();
      GAME.ctx.restore();
    }
    try {
    GAME.ctx.drawImage(sprite.img, 0, 0,
                       sprite.img.naturalWidth, sprite.img.naturalHeight,
                       0, 0, sprite.w, sprite.h);
    }catch(err) {
      console.error("bad sprite?", sprite);
    }
    GAME.ctx.restore();
  });

  // health meters
  GAME.sprites.forEach(function(sprite) {
    if(!sprite.health || sprite.health === 1) return;
    //console.log("drawing health ", sprite.health);

    GAME.ctx.save();
    GAME.ctx.translate(sprite.x + (sprite.w / 2),
                       sprite.y + (sprite.h / 2));
    if(sprite.size || sprite.size === 0) {
      GAME.ctx.scale((sprite.size), (sprite.size));
    }
    GAME.ctx.translate(-sprite.w / 2, -sprite.h / 2);
    GAME.ctx.fillStyle = "darkred";
    GAME.ctx.fillRect(0, 0, sprite.img.naturalWidth, 4);
    GAME.ctx.fillStyle = "green";
    GAME.ctx.fillRect(0, 0, sprite.health * sprite.img.naturalWidth, 4);
    GAME.ctx.restore();
  });

  // draw and discard active shots
  var shot = null;
  GAME.shots.forEach(function(shot) {
    if(!shot || !shot.target || !shot.shooter) {
      return;
    }
    GAME.ctx.strokeStyle = "red";
    GAME.ctx.lineWidth = 4;
    GAME.ctx.beginPath();
    GAME.ctx.moveTo(shot.shooter.x + (shot.shooter.w / 2),
                    shot.shooter.y + (shot.shooter.h / 2));
    GAME.ctx.lineTo(shot.target.x + (shot.target.w / 2),
                    shot.target.y + (shot.target.h / 2));
    GAME.ctx.closePath();
    GAME.ctx.stroke();
    shot.shown = true;
  });

  // hud
  if(GAME.running && LEVEL.time && GAME.end_time < time) {
    if(GAME.justhitpause) {
      // end_time might not be right yet
      GAME.justhitpause = false;
    } else {
      // time is certainly up
      gameover();
    }
  }
  updatetimer(time);

  // see if we've won (or lost)
  checkwin();

  GAME.lastframe = time;
}

// soldier logic
function tick_soldier(sprite, time) {
  sprite.damage = sprite.damage || 0;

  // move around
  meander(sprite, 6);

  // wobble
  sprite.rot = Math.sin((time + sprite.offset) / 100) * 5;

  // every so many ticks, shoot the nearest ant
  if(!((GAME.ticks + sprite.offset) % GAME.config.shoot_interval)) {
    var ant = nearest(sprite, "ant");
    if(ant && ant.shootable && ant.distance < 400) {
      GAME.shots.push({
        shooter: sprite,
        target: ant
      });
    }
  }

  // if there is a chip bag nearby, kill it
  var bag = null;
  if(bag = nearest(sprite, "chipbag")) {
    if(bag.distance < sprite.w) {
      kill(bag, true);
    }
  }

  // if we've taken too much damage, become a wounded soldier
  if(sprite.damage >= GAME.config.tank_hits) {
    kill(sprite, true);
    if(Math.random() < 0.5) {
        GAME.sprites.push({
          img: document.getElementById("woundedsoldier"),
          x: sprite.x,
          y: sprite.y,
          w: document.getElementById("soldier").naturalWidth,
          h: document.getElementById("soldier").naturalHeight
        });
    }
  }
}

// medic logic
function tick_medic(sprite, time) {
  sprite.damage = sprite.damage || 0;

  // wobble
  sprite.rot = Math.sin((time + sprite.offset) / 100) * 5;
/*
  // every so many ticks, shoot the nearest ant
  if(!((GAME.ticks + sprite.offset) % GAME.config.shoot_interval)) {
    var ant = nearest(sprite, "ant");
    if(ant && ant.shootable && ant.distance < 400) {
      GAME.shots.push({
        shooter: sprite,
        target: ant
      });
    }
  }
*/
  target = nearest(sprite, "woundedsoldier");
  if(target && sprite.healing) {
    // if in wounded-healing mode, don't move around
    sprite.healing--;
    sprite.rot += 45;

    // heal quicker than the soldier is dying
    target.health += 0.015;
  } else {
    if(target) {
      // move towards (and above) it
      dist = drive_at(sprite, 2,
                      target.x,
                      Math.max(0, target.y - (target.img.naturalHeight/2)));
      // if we're very close, heal the wounded soldier
      if(dist < target.img.naturalHeight / 8) {
        // go into wounded-healing mode
        if(!sprite.healing) {
          sprite.healing = 24;
        }
      }
    } else {
      // no wounded around, so patrol in any direction
      // move around
      meander(sprite, 6);
    }
  }

  // if we've taken too much damage, die
  if(sprite.damage >= GAME.config.medic_hits) {
    kill(sprite, true);
  }
}

// wounded soldier logic
function tick_woundedsoldier(sprite, time) {
  if(!sprite.health) {
    sprite.health = 1;
  }
  // if we've been healed fully, turn into soldier
  if(sprite.health > 1) {
    GAME.sprites.push({
      img: document.getElementById("soldier"),
      x: sprite.x,
      y: sprite.y,
      w: document.getElementById("soldier").naturalWidth,
      h: document.getElementById("soldier").naturalHeight
    });
    kill(sprite);
    return;
  }
  // slowly lose health
  sprite.health -= 0.005;
  // if health drops to 0, die
  if(sprite.health <= 0) {
    kill(sprite, true);
  }
}

// bunkered soldier logic
function tick_bunkeredsoldier(sprite, time) {
  // every so many ticks, shoot the nearest ant
  if(!((GAME.ticks + sprite.offset) % GAME.config.bunkershoot_interval)) {
    var ant = nearest(sprite, "ant");
    if(ant) {
      GAME.shots.push({
        shooter: sprite,
        target: ant
      });
    }
  }

  // if there is an ant nearby, kill it
  // but also take damage
  var ant = null;
  if(ant = nearest(sprite, "ant")) {
    if(ant.distance < sprite.w) {
      kill(ant, true);
      sprite.damage = sprite.damage || 0;
      sprite.damage++;
      sprite.health = ((GAME.config.tank_hits - sprite.damage) /
                       GAME.config.tank_hits);
    }
  }

  // if we've taken too much damage, become a regular soldier
  if(sprite.damage >= GAME.config.tank_hits) {
    kill(sprite, true);
    GAME.sprites.push({
      img: document.getElementById("soldier"),
      x: sprite.x,
      y: sprite.y,
      w: document.getElementById("soldier").naturalWidth,
      h: document.getElementById("soldier").naturalHeight
    });
  }
}

// ant logic
function tick_ant(sprite, time) {
  // grow when born
  sprite.size = sprite.size || 0;
  sprite.born = sprite.born || time;
  if(sprite.born + 4000 < time) {
    sprite.shootable = true;
  }
  if(sprite.size < 1) {
    sprite.size += (1 / GAME.config.bug_grow_frames);
  }

  // move around
  meander(sprite, 10);

  // wobble
  sprite.rot = Math.sin((time + sprite.offset) / 50) * 10;

  // if there is a soldier nearby, kill it
  var soldier = null;
  if((soldier = nearest(sprite, "soldier"))) {
    if(soldier.distance < sprite.w) {
      soldier.damage++;
    }
  }
  // if there is a medic nearby, kill it
  if((soldier = nearest(sprite, "medic"))) {
    if(soldier.distance < sprite.w) {
      soldier.damage++;
    }
  }
}

// strider logic
function tick_strider(sprite, time) {
  // grow when born
  sprite.size = sprite.size || 0;
  sprite.born = sprite.born || time;
  if(sprite.born + 4000 < time) {
    sprite.shootable = true;
  }
  if(sprite.size < 1) {
    sprite.size += (1 / GAME.config.bug_grow_frames);
  }

  // move around
  meander(sprite, 10);

  // wobble
  sprite.rot = Math.sin((time + sprite.offset) / 50) * 10;

  // if there is a ship nearby, hurt it
  var ship = null;
  if((ship = nearest(sprite, "ship"))) {
    if(ship.distance < sprite.w) {
      ship.damage++;
      kill(sprite, true);
    }
  }
  if((ship = nearest(sprite, "carrier"))) {
    if(ship.distance < sprite.w) {
      ship.damage++;
      kill(sprite, true);
    }
  }
}

// centipede logic
function tick_centipede(sprite, time) {
  // grow when born
  sprite.size = sprite.size || 0;
  sprite.born = sprite.born || time;
  if(sprite.born + 4000 < time) {
    sprite.shootable = true;
  }
  if(sprite.size < 1) {
    sprite.size += (1 / GAME.config.bug_grow_frames);
  }

  // move around
  meander(sprite, 10);

  // wobble
  sprite.rot = Math.sin((time + sprite.offset) / 50) * 10;

  // if there is a dead ant near, revive it and go away
  var ant = null;
  if(ant = nearest(sprite, "deadant")) {
    if(ant.distance < sprite.w) {
      kill(sprite, false);
      kill(ant, false);
      GAME.sprites.push({
        img: document.getElementById("ant"),
        x: sprite.x,
        y: sprite.y,
        w: document.getElementById("ant").naturalWidth,
        h: document.getElementById("ant").naturalHeight
      });
    }
  }
}

// tank logic
function tick_tank(sprite, time) {
  // move slowly in straight line
  drive(sprite, 2);

  // every so many ticks, create a soldier
  if(!((GAME.ticks + sprite.offset) % GAME.config.tanktrooper_interval)) {
    spawn("soldier", sprite.x, sprite.y);
  }

  // if there is an ant nearby, kill it
  // but also take damage
  var ant = null;
  if(ant = nearest(sprite, "ant")) {
    if(ant.distance < sprite.w) {
      kill(ant, true);
      sprite.damage = sprite.damage || 0;
      sprite.damage++;
      sprite.health = ((GAME.config.tank_hits - sprite.damage) /
                       GAME.config.tank_hits);
    }
  }

  // if there is a dead ant nearby, kill it
  if(ant = nearest(sprite, "deadant")) {
    if(ant.distance < sprite.w) {
      kill(ant, true);
    }
  }

  // if we've taken too much damage, become a wrecktank
  if(sprite.damage >= GAME.config.tank_hits) {
    kill(sprite, true);
    GAME.sprites.push({
      img: document.getElementById("wrecktank"),
      x: sprite.x,
      y: sprite.y,
      w: document.getElementById("wrecktank").naturalWidth,
      h: document.getElementById("wrecktank").naturalHeight
    });
  }
}

// wrecked tank logic
function tick_wrecktank(sprite, time) {
  // emit smoke
  make_smoke(sprite);

  // if there is a soldier nearby, turn into a bunkeredsoldier
  var soldier = null;
  if(soldier = nearest(sprite, "soldier")) {
    if(soldier.distance < sprite.w) {
      soldier.damage++;
      //kill(soldier);
      sprite.img = document.getElementById("bunkeredsoldier");
    }
  }

  // if there is an ant nearby, kill it
  // but also take damage
  var ant = null;
  if(ant = nearest(sprite, "ant")) {
    if(ant.distance < sprite.w) {
      kill(ant, true);
      sprite.damage = sprite.damage || 0;
      sprite.damage++;
      sprite.health = ((GAME.config.tank_hits - sprite.damage) /
                       GAME.config.tank_hits);
    }
  }

  // if we've taken too much damage, blow up
  sprite.health = (GAME.config.tank_hits - (sprite.damage || 0)) / GAME.config.tank_hits;
  if(sprite.damage >= GAME.config.tank_hits) {
    kill(sprite, true);
  }
}

// chip bag logic
function tick_chipbag(sprite, time) {
  // every so many ticks, create an ant
  if(!((GAME.ticks + sprite.offset) % GAME.config.bugbag_interval)) {
    spawn("ant", sprite.x + 10, sprite.y + 10, { size: 0 });
  }
}

// support ship logic
function tick_support(sprite, time) {
  // move slowly in straight line
  drive(sprite, 1);

  // if we've taken too much damage, sink
  sprite.damage = sprite.damage || 0;
  sprite.health = ((GAME.config.support_hits - sprite.damage) /
                   GAME.config.support_hits);
  if(sprite.damage >= GAME.config.support_hits) {
    kill(sprite, true);
  }
}

// ship logic
function tick_ship(sprite, time) {
  // move slowly in straight line
  drive(sprite, 2);

  // always show health
  sprite.damage = sprite.damage || 0;
  sprite.health = (GAME.config.ship_hits - sprite.damage) / GAME.config.ship_hits;

  // if we've taken too much damage, sink
  if(sprite.damage >= GAME.config.ship_hits) {
    kill(sprite, true);
  }
}


// carrier logic
function tick_carrier(sprite, time) {
  // move slowly in straight line
  drive(sprite, 2);

  // every so many ticks, create a plane
  if(!((GAME.ticks + sprite.offset) % GAME.config.carrierplane_interval)) {
    if(!sprite.planesOut) {
      sprite.planesOut = 0;
    }
    if(sprite.planesOut < GAME.config.carrier_max_planes) {
      spawn("plane", sprite.x, sprite.y, {
        health: 1,
        size: 0,
        carrier: sprite
      });
      sprite.planesOut++;
    }
  }

  // always show health
  sprite.damage = sprite.damage || 0;
  sprite.health = (GAME.config.carrier_hits - sprite.damage) / GAME.config.carrier_hits;

  // if we've taken too much damage, sink
  if(sprite.damage >= GAME.config.carrier_hits) {
    kill(sprite, true);
    // 15% of the time, create a life boat
    if(Math.random() < 0.15) {
      spawn("lifeboat", sprite.x, sprite.y);
    }
  }
}

// plane logic
function tick_plane(sprite, time) {
  if(!sprite.droppedbomb) {
    if(!sprite.size) {
      sprite.size = 0;
    }
    if(sprite.size < 1) {
      sprite.size += 0.1;
    }
    var target = null;
    var dist = 0;
    // we haven't dropped a bomb yet
    // seek the nearest enemy ship (gunship or support)
    target = nearest(sprite, "gunship");
    if(!target) {
      target = nearest(sprite, "support");
    }
    if(target) {
      // move towards (and above) it
      dist = drive_at(sprite, 4,
                      target.x,
                      Math.max(0, target.y - target.img.naturalHeight));
      // if we're very close, drop a bomb
      if(dist < target.img.naturalHeight / 8) {
        GAME.sprites.push({
          img: document.getElementById("bomb"),
          x: sprite.x,
          y: sprite.y,
          w: document.getElementById("bomb").naturalWidth,
          h: document.getElementById("bomb").naturalHeight,
          target: target
        });
        sprite.droppedbomb = true;
      }
    } else {
      // no enemy ships around, so patrol in any direction
      drive(sprite, 4);
    }
  } else {
    // we have dropped a bomb already, return to the carrier
    target = sprite.carrier;
    if(GAME.sprites.indexOf(sprite.carrier) < 0) {
      // our original carrier is gone!  look for another
      target = nearest(sprite, "carrier");
    }
    if(target) {
      // go towards the carrier
      dist = drive_at(sprite, 4, target.x, target.y);
      if(dist < sprite.img.naturalHeight) {
        sprite.size = dist / sprite.img.naturalHeight;
      }
      // if we're very close, land on the carrier
      if(dist < target.img.naturalHeight / 8) {
        target.planesOut--;
        kill(sprite, false);
        if(LEVEL.player && LEVEL.player.cost &&
           LEVEL.player.cost.plane) {
          spend(-LEVEL.player.cost.plane);
        }
      }
    } else {
      // no carriers anywhere!  so patrol in any direction
      drive(sprite, 4);
    }
  }

  // consume fuel
  if(!sprite.health) sprite.health = 1;
  sprite.health -= (1 / GAME.config.plane_fuel);

  // if we've taken too much damage, blow up
  if(sprite.damage >= GAME.config.plane_hits || sprite.health <= 0) {
    kill(sprite, true);
  }
}

// bomb logic
function tick_bomb(sprite, time) {
  // move towards our target
  if(GAME.sprites.indexOf(sprite.target) < 0) {
    // our target is gone!  fall and crash
    if(!sprite.falling) {
      sprite.falling = 0;
    }
    sprite.falling++;
    sprite.y++;
    if(sprite.falling > sprite.img.naturalHeight) {
      kill(sprite, true);
    }
  } else {
    var dist = drive_at(sprite, 3, sprite.target.x, sprite.target.y);
    sprite.size = dist / sprite.img.naturalHeight;
    if(dist < sprite.target.img.naturalHeight / 8) {
      // kaboom!
      sprite.target.damage = sprite.target.damage || 0;
      sprite.target.damage++;
      kill(sprite, true);
    }
  }
}

// gunship logic
function tick_gunship(sprite, time) {
  // drive around
  drive(sprite, 2);

  // if we've taken too much damage, sink
  sprite.health = (GAME.config.gunship_hits - (sprite.damage || 0)) / GAME.config.gunship_hits;
  if(sprite.damage >= GAME.config.gunship_hits) {
    kill(sprite, true);
  }
}

// advance game state
function tick(time) {
  GAME.ticks++;

  // process any shots, remove targets
  var shot = null;
  while(shot = GAME.shots.shift()) {
    kill(shot.target, true);
    if(shot.target.img === document.getElementById("ant")) {
      GAME.sprites.push({
        img: document.getElementById("deadant"),
        x: shot.target.x,
        y: shot.target.y,
        w: document.getElementById("deadant").naturalWidth,
        h: document.getElementById("deadant").naturalHeight
      });
    }
  }

  // update each game sprite
  GAME.sprites.forEach(function(sprite) {
    // give sprite a random timing offset if it hasn't got one already
    if(!sprite.offset) {
      sprite.offset = Math.floor(Math.random() * 1000);
    }

    // do logic based on what kind of sprite it is
    if(sprite.img === document.getElementById("soldier")) {
      tick_soldier(sprite, time);
    } else if(sprite.img === document.getElementById("bunkeredsoldier")) {
      tick_bunkeredsoldier(sprite, time);
    } else if(sprite.img === document.getElementById("woundedsoldier")) {
      tick_woundedsoldier(sprite, time);
    } else if(sprite.img === document.getElementById("medic")) {
      tick_medic(sprite, time);
    } else if(sprite.img === document.getElementById("ant")) {
      tick_ant(sprite, time);
    } else if(sprite.img === document.getElementById("centipede")) {
      tick_centipede(sprite, time);
    } else if(sprite.img === document.getElementById("tank")) {
      tick_tank(sprite, time);
    } else if(sprite.img === document.getElementById("wrecktank")) {
      tick_wrecktank(sprite, time);
    } else if(sprite.img === document.getElementById("chipbag")) {
      tick_chipbag(sprite, time);
    } else if(sprite.img === document.getElementById("support")) {
      tick_support(sprite, time);
    } else if(sprite.img === document.getElementById("carrier")) {
      tick_carrier(sprite, time);
    } else if(sprite.img === document.getElementById("plane")) {
      tick_plane(sprite, time);
    } else if(sprite.img === document.getElementById("bomb")) {
      tick_bomb(sprite, time);
    } else if(sprite.img === document.getElementById("gunship")) {
      tick_gunship(sprite, time);
    } else if(sprite.img === document.getElementById("ship")) {
      tick_ship(sprite, time);
    } else if(sprite.img === document.getElementById("strider")) {
      tick_strider(sprite, time);
    }

    // boom
    if(sprite.img === document.getElementById("boom")) {
      // emit smoke
      if(GAME.level === "land") {
        make_smoke(sprite);
      } else {
        make_splash(sprite);
      }

      sprite.size += (1 / GAME.config.explosion_frames);
      sprite.solid -= (1 / GAME.config.explosion_frames);
      sprite.framesleft--;
      if(sprite.framesleft <= 0) {
        kill(sprite);
      }
    }
    // smoke/splash
    if(sprite.img === document.getElementById("smoke") ||
       sprite.img === document.getElementById("splash")) {
      sprite.size += (1 / GAME.config.smoke_frames);
      sprite.solid -= (1 / GAME.config.smoke_frames);
      sprite.y -= 3;
      if(sprite.solid < 0) {
        kill(sprite);
      }
    }
  });
}

// sprite helpers
function listAll(type) {
  var list = [];
  GAME.sprites.forEach(function(sprite) {
    if(sprite.img.id === type || !type) {
      list.push(sprite);
    }
  });
  return list;
}
function distance(sprite1, sprite2) {
  try {
    //return Math.abs(sprite1.x - sprite2.x) + Math.abs(sprite1.y - sprite2.y);
    // fixed: pythagoras agrees this is better
    return Math.sqrt((Math.pow(Math.abs(sprite1.x - sprite2.x), 2) +
                      Math.pow(Math.abs(sprite1.y - sprite2.y), 2)));
  } catch(e) {
    console.trace()
  }
}
function nearest(from, type) {
  var list = listAll(type);
  list.forEach(function(sprite) {
    sprite.distance = distance(from, sprite);
  });
  list.sort(function(a, b) {
    return a.distance - b.distance;
  });
  return list[0];
}
function nearest_touch(e) {
  //todo: limit during a level
  var list = listAll();
  list.forEach(function(sprite) {
    sprite.distance = distance({
      x: calcX(e.clientX, e.target) - (sprite.img.naturalWidth / 2),
      y: calcY(e.clientY, e.target) - (sprite.img.naturalHeight / 2)
    }, sprite);
  });
  list.sort(function(a, b) {
    return a.distance - b.distance;
  });
  return list[0];
}
function drive_at(sprite, speed, x, y) {
  var xdiff = x - sprite.x;
  var ydiff = y - sprite.y;
  if(xdiff === 0 && ydiff === 0) {
    sprite.velX = 0;
    sprite.velY = 0;
    return 0;
  }

  if(Math.abs(xdiff) < Math.abs(ydiff)) {
    sprite.velX = xdiff / Math.abs(ydiff) * speed;
    sprite.velY = ydiff / Math.abs(ydiff) * speed;
  } else {
    sprite.velX = xdiff / Math.abs(xdiff) * speed;
    sprite.velY = ydiff / Math.abs(xdiff) * speed;
  }

  drive(sprite, speed);
  return Math.abs(xdiff) + Math.abs(ydiff);
}
function avoidbarricade(sprite) {
  //any barricades around?
  var bar = nearest(sprite, "barricade");
  if(bar && bar.distance < sprite.w) {
    sprite.velX = (sprite.x - bar.x > 0) ? 0.5 : -0.5;
    sprite.velY = (sprite.y - bar.y > 0) ? 0.5 : -0.5;
    stay_on_screen(sprite);
    sprite.x += sprite.velX;
    sprite.y += sprite.velY;
    return true;
  }
}
function vehiclemove(sprite, speed, onsurface) {
  if(onsurface && avoidbarricade(sprite)) return;

  sprite.velX = sprite.velX || (Math.random() * speed) - (speed / 2);
  sprite.velY = sprite.velY || (Math.random() * speed) - (speed / 2);
  if(Math.abs(sprite.velX) + Math.abs(sprite.velY) < speed) {
    if(Math.random() < 0.5) {
      sprite.velX *= 1.1;
    } else {
      sprite.velY *= 1.1;
    }
  }
  sprite.x += sprite.velX;
  sprite.y += sprite.velY;

  stay_on_screen(sprite);
}
function drive(sprite, speed) {
  vehiclemove(sprite, speed, true);
}
function fly(sprite, speed) {
  vehiclemove(sprite, speed, false);
}
function meander(sprite, speed) {
  if(avoidbarricade(sprite)) return;

  // move somewhat randomly
  sprite.velX = sprite.velX || (Math.random() * speed) - (speed / 2);
  sprite.velY = sprite.velY || (Math.random() * speed) - (speed / 2);
  sprite.velX += (Math.random() * (speed / 5)) - (speed / 10);
  sprite.velY += (Math.random() * (speed / 5)) - (speed / 10);
  if(sprite.velX < -(speed / 2)) sprite.velX = -(speed / 2);
  if(sprite.velX > (speed / 2)) sprite.velX = (speed / 2);
  if(sprite.velY < -(speed / 2)) sprite.velY = -(speed / 2);
  if(sprite.velY > (speed / 2)) sprite.velY = (speed / 2);
  sprite.x += sprite.velX;
  sprite.y += sprite.velY;

  stay_on_screen(sprite);
}
function stay_on_screen(sprite) {
  //stay on screen
  if(sprite.x < 0 - sprite.w / 2) {
    sprite.x = 0 - sprite.w / 2;
    sprite.velX *= -1;
  } else if(sprite.x > GAME.ctx.canvas.width - sprite.w / 2) {
    sprite.x = GAME.ctx.canvas.width - sprite.w / 2;
    sprite.velX *= -1;
  }
  if(sprite.y < 0 - sprite.h / 2) {
    sprite.y = 0 - sprite.h / 2;
    sprite.velY *= -1;
  } else if(sprite.y > GAME.ctx.canvas.height - sprite.h / 2) {
    sprite.y = GAME.ctx.canvas.height - sprite.h / 2;
    sprite.velY *= -1;
  }
}
function kill(sprite, withBoom) {
  if(GAME.sprites.indexOf(sprite) >= 0) {
    if(withBoom) {
      GAME.sprites.push({
        img: document.getElementById("boom"),
        x: sprite.x,
        y: sprite.y,
        w: sprite.w,
        h: sprite.h,
        framesleft: GAME.config.explosion_frames,
        size: 1,
        solid: 1
      });
    }
    GAME.sprites.splice(GAME.sprites.indexOf(sprite), 1);
  }
}
function make_smoke(sprite) {
  GAME.sprites.push({
    img: document.getElementById("smoke"),
    x: sprite.x + (Math.random() * sprite.w - sprite.w / 2),
    y: sprite.y + (Math.random() * sprite.h - sprite.h / 2),
    w: document.getElementById("smoke").naturalWidth,
    h: document.getElementById("smoke").naturalHeight,
    solid: 0.5,
    size: 1
  });
}
function make_splash(sprite) {
  GAME.sprites.push({
    img: document.getElementById("splash"),
    x: sprite.x + (Math.random() * sprite.w - sprite.w / 2),
    y: sprite.y + (Math.random() * sprite.h - sprite.h / 2),
    w: document.getElementById("splash").naturalWidth,
    h: document.getElementById("splash").naturalHeight,
    solid: 0.5,
    size: 1
  });
}

// buttons
function pause() {
  GAME.running = !GAME.running;
  GAME.justhitpause = true;
  if(GAME.running) {
    document.querySelector("#pause").textContent = "|| Pause";
    document.querySelector("#pause").className = "";
  } else {
    document.querySelector("#pause").textContent = "|| Resume";
    document.querySelector("#pause").className = "paused";
  }

  if(LEVEL.time) {
    window.requestAnimationFrame(function(now) {
      if(GAME.running) {
        GAME.end_time = now + GAME.msec_left;
      } else {
        GAME.msec_left = GAME.end_time - now;
      }
    });
  }
}

function run(e) {
  GAME.running = !GAME.running;
  if(GAME.running) {
    e.target.textContent = "pause";
    document.querySelector("#tick").className = "hidden";
  } else {
    e.target.textContent = "run";
    document.querySelector("#tick").className = "";
  }
}
function save(e) {
  var json = JSON.stringify(GAME.sprites, function(key, val) {
    if(val instanceof HTMLImageElement) {
      // save img as id
      return val.id;
    }
    return val;
  });
  GAME.backup.sprites = json;
  console.log(json);
}
function load(e) {
  var json = GAME.backup.sprites || "[]";
  if(typeof(e) === "string") {
    // accept a string argument
    json = e;
  } else if(e instanceof Array) {
    // or a json array
    json = JSON.stringify(e);
  }
  GAME.sprites = JSON.parse(json, function(key, val) {
    if(key === "img") {
      // turn img back into an element
      return document.getElementById(val);
    }
    return val;
  });
}
function loadJSON(json) {
  GAME.sprites = [];
  json.forEach(function(sprite) {
    GAME.sprites.push({
      img: document.getElementById(sprite.img),
      x: sprite.x,
      y: sprite.y,
      w: sprite.w,
      h: sprite.h
    });
  });
}
function clear(e) {
  GAME.sprites = [];
  if(document.querySelector("#baserange")) {
    document.querySelector("#baserange").value = "";
  }
}

function basechange(e) {
  var input = e.target;
  var baselist = GAME.sprites.filter(function(sp) {
    return sp.img.id === "base";
  });
  console.log("input value", input.value);
  var radius = parseInt(input.value, null);
  radius = Math.min(radius, 9);
  radius = Math.max(radius, 0);
  input.value = radius;
  if(radius) {
    // make sure there's a base
    if(!baselist.length) {
      var img = document.getElementById("base");
      GAME.sprites.push({
        img: img,
        x: GAME.ctx.canvas.width / 2,
        y: GAME.ctx.canvas.height / 2,
        w: img.naturalWidth,
        h: img.naturalHeight,
        radius: radius
      });
    } else {
      baselist[0].radius = radius;
    }
  } else {
    // make sure there's not a base
    if(baselist.length > 0) {
      console.log("try removing, ", baselist);
      GAME.sprites = GAME.sprites.filter(function(sp) {
        return sp.img.id !== "base";
      });
    }
  }
}

// toggle
function selectlevel(e, skipupdate) {
  GAME.level = document.querySelector("#level").value;

  GAME.sprites = [];
  if(document.querySelector("#baserange")) {
    document.querySelector("#baserange").value = "";
  }

  switch(GAME.level) {
  case "water":
    document.querySelector("#watersprites").className = "";
    document.querySelector("#landsprites").className = "hidden";
    document.querySelector("#base").src = "gpx/base-water.png";
    break;
  case "land":
  default:
    document.querySelector("#landsprites").className = "";
    document.querySelector("#watersprites").className = "hidden";
    document.querySelector("#base").src = "gpx/base.png";
    break;
  }

  // notify the editor, if we're in edit mode
  if(!skipupdate && typeof editupdate === "function") {
    editupdate();
  }
}

// drag-n-drop event handlers
// also handles touch events (well, barely)
function dragstart(e) {
  if(GAME.over) return;
  if(!e.target.draggable || e.target.nodeName !== "IMG") return;
  //console.log("dragstart", e);
  GAME.drag.element = e.target;
  GAME.drag.x = calcX(e.clientX, e.target);
  GAME.drag.y = calcY(e.clientY, e.target);
  e.target.style.backgroundColor = "#123";
  e.target.style.border = "none";
  e.target.style.outline = "thick solid white";
  //console.log(GAME.drag);
}
function touchmove(e) {
  if(GAME.drag.sprite) {
    GAME.drag.sprite.x = calcX(e.changedTouches[0].clientX, e.target);
    GAME.drag.sprite.y = calcX(e.changedTouches[0].clientY, e.target);
    alert("moved", GAME.drag.sprite.x);
    return;
  }
  //console.log("move", e);
  GAME.drag.x = calcX(e.changedTouches[0].clientX, e.target);
  GAME.drag.y = calcX(e.changedTouches[0].clientY, e.target);
  //console.log("x", GAME.drag.x, "y", GAME.drag.y);
  e.target.style.backgroundColor = "#123";
  e.target.style.border = "none";
  e.target.style.outline = "thick solid white";
}
function dragover(e) {
  e.preventDefault();
}
function dragend(e) {
  //console.log("dragend", e);
  e.preventDefault();
}
function drop(e) {
  //console.log("drop");
  if(!GAME.drag.element) return;
  var nope = false;
  //console.log("drop", e);
  if(e.type !== "touchend") {
    e.preventDefault();
  }
  if(GAME.over) return;

  var baselist = GAME.sprites.filter(function(sp) {
    return sp.img.id === "base";
  });
  var added = null;

  if(e.target === GAME.ctx.canvas) {
    if(LEVEL.player && LEVEL.player.cost) {
      if(!spend(LEVEL.player.cost[GAME.drag.element.id])) {
        console.log("not enough coins");
        nope = true;
      }
    }
    if(!nope) {
      GAME.sprites.push({
        img: GAME.drag.element,
        x: calcX(e.clientX, GAME.ctx.canvas) - GAME.drag.x,
        y: calcY(e.clientY, GAME.ctx.canvas) - GAME.drag.y,
        w: GAME.drag.element.naturalWidth,
        h: GAME.drag.element.naturalHeight
      });
      added = GAME.sprites[GAME.sprites.length - 1]
    }
  } else if(e.target.nodeName === "IMG" && e.changedTouches) {
    console.log("plop at", e.changedTouches[0].clientX, e.changedTouches[0].clientY);
    if(LEVEL.player && LEVEL.player.cost) {
      if(!spend(LEVEL.player.cost[GAME.drag.element.id])) {
        console.log("not enough coins");
        nope = true;
      }
    }
    if(!nope) {
      //console.log("pushing", e);
      var x = calcX(e.changedTouches[0].clientX, GAME.ctx.canvas) - GAME.drag.element.naturalWidth/2;// - GAME.drag.x
      var y = calcY(e.changedTouches[0].clientY, GAME.ctx.canvas) - GAME.drag.element.naturalHeight/2;// - GAME.drag.y
      //console.log(x, y);

      if(x >= 0 && x <= GAME.ctx.canvas.width &&
         y >= 0 && y <= GAME.ctx.canvas.height) {
        GAME.sprites.push({
          img: GAME.drag.element,
          x: x,
          y: y,
          w: GAME.drag.element.naturalWidth,
          h: GAME.drag.element.naturalHeight
        });
        added = GAME.sprites[GAME.sprites.length - 1]
      } else {
        console.log("out of range; not dropping.");
      }
    }
  }

  //restrict to base radius if needed
  if(added && baselist.length > 0) {
    //console.log("Have a base");
    if(LEVEL.play) {
      var dist = 0;
      iter = 0;
      while(iter < 10 && ((dist = distance(baselist[0], added)) >
             (baselist[0].radius
              * baselist[0].img.naturalWidth
              * GAME.config.base_radius_scale))) {
        iter++;
        //console.log("restricting, dist=", dist);
        //console.log("x", baselist[0].x, added.x);
        //console.log("y", baselist[0].y, added.y);
        added.x = (baselist[0].x + added.x) / 2;
        added.y = (baselist[0].y + added.y) / 2;
      }
    }
  }
  
  GAME.drag.element.style.outline = "none";
  GAME.drag.element.style.backgroundColor = "#0000";
  GAME.drag.element = null;

  // notify the editor, if we're in edit mode
  if(typeof editupdate === "function") {
    editupdate();
  }
}

function mousedown(e) {
  if(e.target === GAME.ctx.canvas && GAME.sprites.length) {
    // special case: pick up a sprite from the playfield
    GAME.drag.sprite = nearest_touch(e);
    return;
  }
}
function mousemove(e) {
  if(GAME.drag.sprite) {
    if(e.changedTouches) {
      //GAME.drag.sprite.x = calcX(e.changedTouches[0].clientX, GAME.ctx.canvas);
      //GAME.drag.sprite.y = calcX(e.changedTouches[0].clientY, GAME.ctx.canvas);
      GAME.drag.sprite.x = e.changedTouches[0].clientX;
      GAME.drag.sprite.y = e.changedTouches[0].clientY;
    } else {
      GAME.drag.sprite.x = calcX(e.clientX, GAME.ctx.canvas) - (GAME.drag.sprite.img.naturalWidth/2);
      GAME.drag.sprite.y = calcY(e.clientY, GAME.ctx.canvas) - (GAME.drag.sprite.img.naturalHeight/2);
    }
  }
}
function mouseup(e) {
  if(GAME.drag.sprite) {
    // delete+refund if off field
    var x = e.clientX - GAME.ctx.canvas.offsetLeft;
    var y = e.clientY - GAME.ctx.canvas.offsetTop;
    if(x < 0 || x > GAME.ctx.canvas.offsetWidth ||
       y < 0 || y > GAME.ctx.canvas.offsetHeight) {
      console.log("deleting ", x, y);
      if(LEVEL.player && LEVEL.player.cost) {
        // refund
        spend(-LEVEL.player.cost[GAME.drag.sprite.img.id]);
      }
      GAME.sprites = GAME.sprites.filter(function(spr) {
        return spr !== GAME.drag.sprite;
      });
    }
    GAME.drag.sprite = null;
  }
}
function mouseout(e) {
  if(GAME.drag.sprite) {
    // delete+refund
    if(LEVEL.player && LEVEL.player.cost) {
      // refund
      spend(-LEVEL.player.cost[GAME.drag.sprite.img.id]);
    }
    if(GAME.drag.sprite.img.id === "base") {
      if(document.querySelector("#baserange")) {
        document.querySelector("#baserange").value = "";
      }
    }
    GAME.sprites = GAME.sprites.filter(function(spr) {
      return spr !== GAME.drag.sprite;
    });
    GAME.drag.sprite = null;
  }
}

function imgtap(e) {
  if(GAME.over) return;
  if(!e.target.draggable) return;

  if(LEVEL.player && LEVEL.player.cost) {
    if(!spend(LEVEL.player.cost[e.target.id])) {
      console.log("not enough coins");
      return;
    }
  }

  GAME.sprites.push({
    img: e.target,
    x: Math.random() * GAME.ctx.canvas.width,
    y: Math.random() * GAME.ctx.canvas.height,
    w: e.target.naturalWidth,
    h: e.target.naturalHeight
  });

  // notify the editor, if we're in edit mode
  if(typeof editupdate === "function") {
    editupdate();
  }
}

// helpers for scaled image handling
function calcX(clientX, target) {
  return Math.round((clientX - target.offsetLeft) *
                    ((target.naturalWidth || target.width) /
                     target.clientWidth));
}
function calcY(clientY, target) {
  return Math.round((clientY - target.offsetTop) *
                    ((target.naturalHeight || target.height) /
                     target.clientHeight));
}

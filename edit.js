/* include AFTER game.js, we overwrite LEVEL */

var LEVEL = {
  about: "",
  setting: "land",
  time: 0,
  required: null,
  player: {
    coins: 99,
    cost: {},
    placed: []
  },
  enemy: {
    placed: []
  }
};
var LEVEL_stock = JSON.parse(JSON.stringify(LEVEL));

var INPUT = {
  name: null,
  about: null,
  time_min: null,
  time_sec: null,
  required: null,
  coins: null,
  cost: {},
  spec: null
};

var PLAYER_AVAILABLE = [
  "soldier", "medic", "injuredsoldier", "tank", "ship", "carrier", "plane", "bunkeredsoldier", "barricade", "base"
];

function pick(e) {
  LEVEL = JSON.parse(this.value);
  INPUT.name.value = this.selectedOptions[0].textContent;
  updatespec();
  INPUT.spec.dispatchEvent(new Event("change"));
  this.value = "_load";
}

function populatelist(select, collection, label) {
  while(select.firstChild) select.removeChild(select.firstChild);
  var opt = document.createElement("option");
  opt.value = "_load";
  opt.textContent = label;
  select.appendChild(opt);
  for(level in collection) {
    opt = document.createElement("option");
    opt.value = JSON.stringify(collection[level]);
    opt.textContent = level;
    select.appendChild(opt);
  }
}
function populaterequired(select) {
  while(select.firstChild) select.removeChild(select.firstChild);

  var opt = document.createElement("option");
  var optgroup = null;
  opt.value = "";
  opt.textContent = "(none)";
  select.appendChild(opt);

  var all = {"Game Levels": LEVELS, "Custom Levels": DATA.custom};
  for(collection in all) {
    optgroup = document.createElement("optgroup");
    optgroup.label = collection;
    for(level in all[collection]) {
      opt = document.createElement("option");
      opt.value = level;
      opt.textContent = level;
      optgroup.appendChild(opt);
    }
    select.appendChild(optgroup);
  }
}

window.addEventListener("load", function() {
  if(typeof data_load === "function") {
    data_load();
  }

  document.querySelector("#clear").addEventListener("click", function() {
    LEVEL = JSON.parse(JSON.stringify(LEVEL_stock));
    updatespec();
    INPUT.name.value = "";
    INPUT.spec.dispatchEvent(new Event("change"));
  });

  var sel = document.querySelector("#loadfromgame");
  sel.addEventListener("change", pick);
  populatelist(sel, LEVELS, "Load from game...");
  sel = document.querySelector("#loadfromdata");
  sel.addEventListener("change", pick);
  populatelist(sel, DATA.custom, "Load from save data...");

  INPUT.required = document.querySelector("#required");
  INPUT.required.addEventListener("change", function(e) {
    LEVEL.required = this.value || null;
    updatespec();
  });
  populaterequired(INPUT.required);

  document.querySelector("button.testlevel").addEventListener("click", test);
  document.querySelector("button.delete").addEventListener("click", remove);
  document.querySelector("button.savetodata").addEventListener("click", save);
  document.querySelector("button.download").addEventListener("click", download);
  document.querySelector("button.uploadlvl").addEventListener("click", upload);
  var input = document.querySelector("input.uploadlvl");
  input.addEventListener("change", function(e) {
	var reader = new FileReader();
	reader.addEventListener("load", function(e) {
      var filename = input.value;
      filename = filename.substring(filename.lastIndexOf("\\") + 1);
      filename = filename.substring(filename.lastIndexOf("/") + 1);
      filename = filename.replace(".json", "");
      INPUT.name.value = filename;
	  INPUT.spec.value = e.target.result;
      INPUT.spec.dispatchEvent(new Event("change"));
	});
	reader.readAsText(this.files[0]);
  });

  INPUT.name = document.querySelector("input.name");

  INPUT.about = document.querySelector("input.about"), "about";
  INPUT.about.addEventListener("change", function(e) {
    LEVEL.about = this.value;
    updatespec();
  });
  INPUT.about.value = LEVEL.about;

  INPUT.time_min = document.querySelector("input.minutes");
  INPUT.time_min.addEventListener("change", function(e) {
    LEVEL.time = parseInt(this.value) * 60;
    LEVEL.time += parseInt(INPUT.time_sec.value);
    updatespec();
  });
  INPUT.time_min.value = Math.floor(LEVEL.time / 60);

  INPUT.time_sec = document.querySelector("input.seconds");
  INPUT.time_sec.addEventListener("change", function(e) {
    LEVEL.time = parseInt(INPUT.time_min.value) * 60;
    LEVEL.time += parseInt(this.value);
    updatespec();
  });
  INPUT.time_sec.value = Math.floor(LEVEL.time % 60);

  INPUT.coins = document.querySelector("input.coins");
  INPUT.coins.addEventListener("change", function(e) {
    LEVEL.player.coins = parseInt(this.value);
    updatespec();
  });
  INPUT.coins.value = LEVEL.player.coins;

  INPUT.spec = document.querySelector("textarea.spec");
  INPUT.spec.addEventListener("change", function(e) {
    try {
      LEVEL = JSON.parse(this.value), INPUT.name.value;
      INPUT.about.value = LEVEL.about;
      INPUT.time_min.value = Math.floor(LEVEL.time / 60);
      INPUT.time_sec.value = Math.floor(LEVEL.time % 60);
      INPUT.required.value = LEVEL.required || "";
      INPUT.coins.value = LEVEL.player.coins;
      for(key in INPUT.cost) {
        INPUT.cost[key].value = LEVEL.player.cost[key] || 0;
      }

      loadlevel(LEVEL); //game.js
    } catch(err) {
      alert("Error loading level.\n" + err);
    }
  });

  costsform();
  updatespec();
});

function costsform() {
  var items = document.querySelectorAll("img[draggable=true]");
  items.forEach(function(item) {
    var id = item.id;
    var cont = item.parentElement;
    var span = document.createElement("span");
    span.className = "buy";
    span.appendChild(item);
    span.appendChild(document.createElement("br"));

    var input = document.createElement("input");
    input.className = "cost";
    input.type = "number";
    input.min = 0;
    input.max = 99999;
    input.value = 0;
    span.appendChild(input);
    INPUT.cost[id] = input;
    input.addEventListener("change", function(e) {
      LEVEL.player.cost[id] = parseInt(this.value);
      if(this.value === "0") {
        delete LEVEL.player.cost[id];
      }
      updatespec();
    });

    cont.appendChild(span);
  });
}

function editupdate() {
//  if(!GAME || !GAME.level || !LEVEL || !LEVEL.player) return;

  LEVEL.setting = GAME.level;

  LEVEL.player.placed = [];
  LEVEL.enemy.placed = [];
  GAME.sprites.forEach(function(sprite) {
    var target = LEVEL.enemy;
    if(PLAYER_AVAILABLE.indexOf(sprite.img.id) >= 0) {
      target = LEVEL.player;
    }
    target.placed.push({
      type: sprite.img.id,
      x: sprite.x / GAME.ctx.canvas.width,
      y: sprite.y / GAME.ctx.canvas.height,
      radius: sprite.radius || undefined
    })
  });

  updatespec();
}

function updatespec() {
  if(!INPUT.spec) {
    setTimeout(function() {
      updatespec();
    }, 100);
  } else {
    INPUT.spec.value = JSON.stringify(LEVEL, null, 2);
  }
}

function download() {
  var a = null;
  a = document.createElement("a");
  a.className = "download hidden";
  a.download = INPUT.name.value + ".json";
  a.href = "data:text/plain;base64," + btoa(JSON.stringify(LEVEL, null, 2));
  document.body.appendChild(a);
  document.querySelector("a.download").click();
  document.body.removeChild(a);
}

function upload() {
  document.querySelector("input.uploadlvl").click();
}

function test() {
  var level = encodeURIComponent(JSON.stringify(LEVEL));
  var name = encodeURIComponent(INPUT.name.value);
  window.open("play.html?level=" + level + "&title=" + name);
}

function save() {
  if(!INPUT.name.value) {
    alert("Level needs a name before it can be saved.");
    return;
  }
  if(DATA) {
    DATA.custom = DATA.custom || {};
    DATA.custom[INPUT.name.value] = LEVEL;
  }
  if(typeof data_save === "function") {
    data_save();
  }
  populatelist(document.querySelector("#loadfromdata"),
               DATA.custom, "Load from save data...");
  populaterequired(INPUT.required);
}

function remove() {
  if(INPUT.name.value && DATA.custom[INPUT.name.value] &&
     confirm("Confirm: delete this custom level?")) {
    delete DATA.custom[INPUT.name.value];
    if(typeof data_save === "function") {
      data_save();
    }
    populatelist(document.querySelector("#loadfromdata"),
                 DATA.custom, "Load from save data...");
    populaterequired(INPUT.required);
    document.querySelector("#clear").dispatchEvent(new Event("click"));
  }
}
